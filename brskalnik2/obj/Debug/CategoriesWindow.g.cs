﻿#pragma checksum "..\..\CategoriesWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "A398D427A75829BF2A512F7BD6059F034617DE3E"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using brskalnik2;


namespace brskalnik2 {
    
    
    /// <summary>
    /// CategoriesWindow
    /// </summary>
    public partial class CategoriesWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 14 "..\..\CategoriesWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button socijal_networks;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\CategoriesWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button free_time;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\CategoriesWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button multimedia;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\CategoriesWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button other;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\CategoriesWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox kategorija;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\CategoriesWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button add;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\CategoriesWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox zaznamek_ime;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\CategoriesWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox zaznamek_povezava;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\CategoriesWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox tab_bar_add;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\CategoriesWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button cancel;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/brskalnik2;component/categorieswindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\CategoriesWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 10 "..\..\CategoriesWindow.xaml"
            ((brskalnik2.CategoriesWindow)(target)).Loaded += new System.Windows.RoutedEventHandler(this.KWindow_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.socijal_networks = ((System.Windows.Controls.Button)(target));
            
            #line 14 "..\..\CategoriesWindow.xaml"
            this.socijal_networks.Click += new System.Windows.RoutedEventHandler(this.izbral_kategorijo);
            
            #line default
            #line hidden
            return;
            case 3:
            this.free_time = ((System.Windows.Controls.Button)(target));
            
            #line 15 "..\..\CategoriesWindow.xaml"
            this.free_time.Click += new System.Windows.RoutedEventHandler(this.izbral_kategorijo);
            
            #line default
            #line hidden
            return;
            case 4:
            this.multimedia = ((System.Windows.Controls.Button)(target));
            
            #line 16 "..\..\CategoriesWindow.xaml"
            this.multimedia.Click += new System.Windows.RoutedEventHandler(this.izbral_kategorijo);
            
            #line default
            #line hidden
            return;
            case 5:
            this.other = ((System.Windows.Controls.Button)(target));
            
            #line 17 "..\..\CategoriesWindow.xaml"
            this.other.Click += new System.Windows.RoutedEventHandler(this.izbral_kategorijo);
            
            #line default
            #line hidden
            return;
            case 6:
            this.kategorija = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.add = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\CategoriesWindow.xaml"
            this.add.Click += new System.Windows.RoutedEventHandler(this.izbral_kategorijo);
            
            #line default
            #line hidden
            return;
            case 8:
            this.zaznamek_ime = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.zaznamek_povezava = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.tab_bar_add = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 11:
            this.cancel = ((System.Windows.Controls.Button)(target));
            
            #line 39 "..\..\CategoriesWindow.xaml"
            this.cancel.Click += new System.Windows.RoutedEventHandler(this.cancel_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

