﻿using brskalnik2.classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace brskalnik2
{
    /// <summary>
    /// Interaction logic for CategoriesWindow.xaml
    /// </summary>
    public partial class CategoriesWindow : Window
    {
        public CategoriesWindow()
        {
            InitializeComponent();
        }

        private void KWindow_Loaded(object sender, RoutedEventArgs e)
        {
            MainWindow main = (MainWindow)Application.Current.MainWindow;
            MainWindow.Browsertab izbran_tab = main.browser_tabs.SelectedItem as MainWindow.Browsertab;
            zaznamek_ime.Text = izbran_tab.wb.Source.Authority;
            zaznamek_povezava.Text = izbran_tab.wb.Source.AbsoluteUri;
        }


        private void izbral_kategorijo(object sender, RoutedEventArgs e)
        {

            Button posiljatelj = sender as Button;

            MainWindow main = (MainWindow)Application.Current.MainWindow;
            MainWindow.Browsertab izbran_tab = main.browser_tabs.SelectedItem as MainWindow.Browsertab;

            Zazn trenutna = new Zazn(izbran_tab.wb.Source.Authority, izbran_tab.wb.Source.AbsoluteUri);

            if (posiljatelj.Name == "add")
            {
                if(kategorija.Text== "napisi ime ...")
                {
                    trenutna.type = "other";
                }
                else
                {
                    trenutna.type = kategorija.Text;
                }  
            }
            else
            {
                trenutna.type = posiljatelj.Name.ToString();
            }
           

            WebBrowser preveri = new WebBrowser();
            bool napaka = false;
            try
            {
                if (izbran_tab.wb.Source.Authority.Contains("www."))
                {
                    preveri.Navigate(new Uri("http://" + izbran_tab.wb.Source.Authority + "/favicon.ico"));
                }
                else
                {
                    preveri.Navigate(new Uri("http://www." + izbran_tab.wb.Source.Authority + "/favicon.ico"));
                }
            }
            catch
            {
                napaka = true;
            }

            if (napaka == false)
            {
                if (izbran_tab.wb.Source.Authority.Contains("www."))
                {
                    trenutna.Picture_path = "http://" + izbran_tab.wb.Source.Authority + "/favicon.ico";
                }
                else
                {
                    trenutna.Picture_path = "http://www." + izbran_tab.wb.Source.Authority + "/favicon.ico";
                }
            }
            else
            {
                trenutna.Picture_path = "icons/png/link.png";
            }



            if (tab_bar_add.IsChecked == true)
            {
                Shrani.Add_to_zazn_tab(trenutna);
                main.napolni_zazn_tab();
            }

            Shrani.Add_to_zazn(trenutna);
            this.Close();

        }


       


        //private void potrdi_Click(object sender, RoutedEventArgs e)
        //{ 


        //    MainWindow main = (MainWindow)Application.Current.MainWindow;
        //    MainWindow.Browsertab izbran_tab = main.browser_tabs.SelectedItem as MainWindow.Browsertab;

        //    Zazn trenutna = new Zazn(izbran_tab.wb.Source.Authority, izbran_tab.wb.Source.AbsoluteUri);
        //    trenutna.type = "other";



        //}

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


    }
}
