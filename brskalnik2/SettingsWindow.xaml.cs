﻿using brskalnik2.classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace brskalnik2
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        public SettingsWindow()
        {
            InitializeComponent();
        }

        private void preklici_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            set_homepage.Text = MainWindow.zapisi.kako_zgleda.homepage;
        }

        private void potrdi_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.zapisi.kako_zgleda.homepage = set_homepage.Text;
            if (zagon_programa.IsChecked == true)
            {
                MainWindow.zapisi.kako_zgleda.show_bar = true;
            }
            else
            {
                MainWindow.zapisi.kako_zgleda.show_bar = false;
            }
            Shrani.napisi_vse();

            //this.DialogResult = true;
        }

        private void set_trenutna_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = (MainWindow)Application.Current.MainWindow;
            MainWindow.Browsertab trenuten = main.browser_tabs.SelectedItem as MainWindow.Browsertab;

            set_homepage.Text = trenuten.wb.Source.ToString();
        }
    }
}
