﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using brskalnik2.classes;

namespace brskalnik2
{
    
    public partial class HistoryBookmarksWindow : Window
    {

        private ObservableCollection<Tip_zaznamka> kategorije =new ObservableCollection<Tip_zaznamka>();



        public void naredi_vrsto()
        {
            foreach(Zazn i in MainWindow.zapisi.list_zaznamkov)
            {
                bool postavljen = false;
                foreach(Tip_zaznamka cat in kategorije)
                {
                    if (i.type == cat.kategorija)
                    {
                        cat.zaznamki_kategorije.Add(i);
                        postavljen = true;
                    }
                }
                if (postavljen == false)
                {
                    Tip_zaznamka zacasen = new Tip_zaznamka();
                    zacasen.kategorija = i.type;
                    zacasen.zaznamki_kategorije.Add(i);
                    kategorije.Add(zacasen);
                }
            }

        }



        public HistoryBookmarksWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //izprazni();
            //napolni_zaznamke();

            naredi_vrsto();

            zgodovina.ItemsSource = MainWindow.zapisi.list_zgodovina;
            zgodovina.MouseDoubleClick += izbral;

            CategoryList.ItemsSource = kategorije;
            zgodovina.KeyDown += izbrisi;
            
            
        }




        private void izbrisi(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                while(zgodovina.SelectedItems.Count > 0)
                {
                    MainWindow.zapisi.list_zgodovina.Remove((Zgoda)zgodovina.SelectedItem);
                }
                zgodovina.Items.Refresh();

            }

        }




        private void izbral(object sender, MouseButtonEventArgs e)
        {
            var item = (sender as ListView).SelectedItem;
            if (item != null)
            {
                Zgoda zacasen = zgodovina.SelectedItem as Zgoda;
                MainWindow main = (MainWindow)Application.Current.MainWindow;
                MainWindow.Browsertab trenuten = main.browser_tabs.SelectedItem as MainWindow.Browsertab;
                
                trenuten.wb.Source = new Uri(zacasen.Web_url, UriKind.Absolute);
                this.Close();
                //MessageBox.Show(zacasen.Print());
            }
            
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button zacasen = sender as Button;

            MainWindow main = (MainWindow)Application.Current.MainWindow;
            MainWindow.Browsertab trenuten = main.browser_tabs.SelectedItem as MainWindow.Browsertab;

            trenuten.wb.Source = new Uri(zacasen.Tag.ToString(), UriKind.Absolute);
            this.Close();
               
            

        }

        private void izbrisi_vse_Click(object sender, RoutedEventArgs e)
        {
            var exit = MessageBox.Show("Res pobrisem vso zgodovino?", "brisanje", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (exit == MessageBoxResult.Yes)
            {
                MainWindow.zapisi.list_zgodovina.Clear();
                Shrani.napisi_vse();
                zgodovina.Items.Refresh();
            }
        }

        public TextBlock posiljatel;
        private void kati_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            posiljatel = sender as TextBlock;
            if (posiljatel != null)
            {
                ContextMenu cm = menu_pomoc as ContextMenu;
                cm.PlacementTarget = sender as TextBlock;

                cm.IsOpen = true;


                

            }
        }

        private void izbrisi_zazn_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("brisem vse" + posiljatel.Text);
            //ObservableCollection<Zazn> zacasna = new ObservableCollection<Zazn>();

            //foreach(Zazn i in MainWindow.zapisi.list_zaznamkov)
            //{
            //    if(!(i.type.Contains(posiljatel.Text)))
            //    {
            //        zacasna.Add(i);
            //    }
            //    else{
            //        MessageBox.Show("pos " + posiljatel.Text + " in typ " + i.type);
            //    }
            //}
            //MainWindow.zapisi.list_zaznamkov = zacasna;
            //zacasna.Clear();

            //foreach (Zazn i in MainWindow.zapisi.list_zaznamkov_tab)
            //{
            //    if (i.type != posiljatel.Text)
            //    {
            //        zacasna.Add(i);
            //    }
            //}
            //MainWindow.zapisi.list_zaznamkov_tab = zacasna;
            //zacasna.Clear();

            //kategorije.Clear();
            //naredi_vrsto();
            //Shrani.napisi_vse();
            //CategoryList.Items.Refresh();
        }
    }
}
