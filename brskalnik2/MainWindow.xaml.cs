﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using brskalnik2.classes;
using System.Drawing;
using System.Windows.Media.Animation;
using Microsoft.Win32;

namespace brskalnik2
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 


    public partial class MainWindow : Window
    {
        public static Shrani zapisi = new Shrani();

        public TabControl tab {
            get { return browser_tabs; }
            set { browser_tabs = value; }
        }
        public TextBlock stat
        {
            get { return status; }
            set { status = value; }
        }
        public MainWindow()
        {
            InitializeComponent();
            

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Shrani.preberi_vse();
            Metoda_dodaj_okno();
            napolni_zazn_tab();
            pripravi_okno();
            //WindowState= WindowState.Maximized;
        }


        public void pripravi_okno()
        {
            if (MainWindow.zapisi.kako_zgleda.show_bar == false)
            {
                menu_dock.Visibility = Visibility.Collapsed;
                UpdateLayout();
            }
        }

        public void napolni_zazn_tab()
        {
            tab_bar.ItemsSource = MainWindow.zapisi.list_zaznamkov_tab;
           
        }

        private void single_zazn_Click(object sender, RoutedEventArgs e)
        {
            Button izbran = sender as Button;
            Uri pojdi_sem = new Uri(izbran.Tag.ToString(), UriKind.Absolute);
            Browsertab izbran_tab = browser_tabs.SelectedItem as Browsertab;
            izbran_tab.wb.Source = pojdi_sem;

        }


        private void Exit_app(object sender, RoutedEventArgs e)
        {
            var exit = MessageBox.Show("Do you really want to close the browser?", "exiting", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (exit == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }

        private void search_button_Click(object sender, RoutedEventArgs e)
        {
            Uri pojdi_sem=Preveri_link.Is_link(url_text.Text);
            Browsertab izbran_tab = browser_tabs.SelectedItem as Browsertab;
            izbran_tab.wb.Source = pojdi_sem;
        }

        private void search_enter(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                Uri pojdi_sem = Preveri_link.Is_link(url_text.Text);
                Browsertab izbran_tab = browser_tabs.SelectedItem as Browsertab;
                izbran_tab.wb.Source = pojdi_sem;
            }

        }


        private void Add_bookmarks_Click(object sender, RoutedEventArgs e)
        {
            Browsertab izbran_tab = browser_tabs.SelectedItem as Browsertab;
            foreach (Zazn i in MainWindow.zapisi.list_zaznamkov)
            {
                if (izbran_tab.wb.Source.AbsoluteUri == i.Web_url)
                {
                    MessageBox.Show("Ze obstaja!", "zaznamki");
                    return;
                }
            }

            CategoriesWindow okno = new CategoriesWindow();
            okno.ShowDialog();


        }

        private void bookmarks_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("did it work", "message", MessageBoxButton.OK, MessageBoxImage.Question);
        }

        private void tools_Click(object sender, RoutedEventArgs e)
        {
            SettingsWindow set = new SettingsWindow();
            set.ShowDialog();
            //if (set.DialogResult.Equals(true))
            //{
            //    if (set.menu_prikazi.IsChecked == false)
            //    {
            //        menu_dock.Visibility = Visibility.Collapsed;
            //        UpdateLayout();
            //        //MessageBox.Show("did it work", "message", MessageBoxButton.OK, MessageBoxImage.Question);
            //    }

            //}

        }


        private void History_open(object sender, RoutedEventArgs e)
        {
            HistoryBookmarksWindow his = new HistoryBookmarksWindow();
            if (e.Source.Equals(zgodovina_odpri))
            {
                his.okno_zgo_zaz.SelectedIndex = 0;
            }
            if (e.Source.Equals(zaznamki_odpri))
            {
                his.okno_zgo_zaz.SelectedIndex = 1;
            }
            his.ShowDialog();
        }


        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Alt)
            {
                if (menu_dock.Visibility == Visibility.Collapsed)
                {
                    menu_dock.Visibility = Visibility.Visible;
                    UpdateLayout();
                }
                else
                {
                    menu_dock.Visibility = Visibility.Collapsed;
                    UpdateLayout();
                }

            }

            
        }







        public class Browsertab : TabItem
        {
            public WebBrowser wb = new WebBrowser();
            WrapPanel naslov_zavihka = new WrapPanel();
            public Button btn = new Button();
            TextBlock ime_strani = new TextBlock();
            

            ~Browsertab()
            {
                
            }

            public Browsertab()
            {
                //Header_strani("tole");
                wb.Navigated += Zamenjal_sem_stran;
                wb.Navigating += Menjam_stran;
                wb.Source = new Uri(MainWindow.zapisi.kako_zgleda.homepage,UriKind.Absolute);
                wb.HorizontalAlignment = HorizontalAlignment.Stretch;
                wb.VerticalAlignment = VerticalAlignment.Stretch;
                this.Content = wb;
                


            }


            public void Zamenjal_sem_stran(object sender, System.EventArgs e)
            {


                MainWindow main = (MainWindow)Application.Current.MainWindow;
                var webBrowser = sender as WebBrowser;
                Browsertab izbran_tab = main.tab.SelectedItem as Browsertab;
                TextBlock status = main.stat as TextBlock;
                Header_strani(webBrowser.Source.Authority.ToString());
                status.Text ="loaded: "+ webBrowser.Source.AbsoluteUri;
                main.url_text.Text = webBrowser.Source.AbsoluteUri;

                Zgoda zacasna = new Zgoda(webBrowser.Source.Authority.ToString(), webBrowser.Source.AbsoluteUri.ToString());
                Shrani.Add_to_zgod(zacasna);
                Zgoda.stevilo_zgodovin++;
                //main.vsak_status.Height = new GridLength(0);
                animacija(false);
            }


            public void Menjam_stran(object sender, System.EventArgs e)
            {
                MainWindow main = (MainWindow)Application.Current.MainWindow;
                TextBlock status = main.stat as TextBlock;
                //main.nosilec_statusa.Visibility = Visibility.Visible;
                //main.vsak_status.Height = new GridLength(18);
                status.Text ="loading....";


                animacija(true);


            }

            public void animacija(bool kdaj)
            {
                MainWindow main = (MainWindow)Application.Current.MainWindow;
                DoubleAnimation da = new DoubleAnimation(360, 0, new Duration(TimeSpan.FromSeconds(1)));
                RotateTransform rt = new RotateTransform();
                main.loading_image.RenderTransform = rt;
                main.loading_image.RenderTransformOrigin = new Point(0.5, 0.5);
                da.RepeatBehavior = RepeatBehavior.Forever;
                //rt.BeginAnimation(RotateTransform.AngleProperty, da);

                if (kdaj)
                {
                    rt.BeginAnimation(RotateTransform.AngleProperty, da);

                }
                else
                {
                    rt.BeginAnimation(RotateTransform.AngleProperty, null);
                }


            }


            public void zapri_stran(object sender, System.EventArgs e)
            {
                MainWindow main = (MainWindow)Application.Current.MainWindow;

                if (main.browser_tabs.Items.Count==1 ) {
                    var exit = MessageBox.Show("Do you really want to close the browser?", "exiting", MessageBoxButton.YesNo, MessageBoxImage.Question);

                    if (exit == MessageBoxResult.Yes)
                    {
                        Application.Current.Shutdown();
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {

                    this.Dispose();
                    main.browser_tabs.Items.Remove(this);
                }
            }


            public void Header_strani(string kam)
            {

                naslov_zavihka.Children.Remove(ime_strani);
                naslov_zavihka.Children.Remove(btn);
                this.btn.Click += zapri_stran;
                Image zapri_slikca = new Image();
                zapri_slikca.Source = new BitmapImage(new Uri(@"icons/icons/error.png", UriKind.Relative));
                zapri_slikca.Height = 20;
                zapri_slikca.Width = 20;
                this.btn.BorderBrush = Brushes.Transparent;
                this.btn.Background = Brushes.Transparent;

                this.btn.Content = zapri_slikca;

                ime_strani.Text = kam;
                ime_strani.Width = 80;
                ime_strani.Height = 18;
                naslov_zavihka.Children.Add(ime_strani);
                naslov_zavihka.Children.Add(btn);
                
                this.Header = naslov_zavihka;
            }

            public void Dispose()
            {
                wb.Dispose();
            }
        }

        





        public void Metoda_dodaj_okno()
        {
            Browsertab nov_zavihek = new Browsertab();
            browser_tabs.Items.Add(nov_zavihek);
        }


        public void add_tab_Click(object sender, RoutedEventArgs e)
        {
            Metoda_dodaj_okno();
        }



        private void Go_back(object sender, RoutedEventArgs e)
        {
            Browsertab izbran_tab = browser_tabs.SelectedItem as Browsertab;
            if (izbran_tab.wb.CanGoBack)
            {
                izbran_tab.wb.GoBack();
            }
        }

        private void Go_forward(object sender, RoutedEventArgs e)
        {
            Browsertab izbran_tab = browser_tabs.SelectedItem as Browsertab;
            if (izbran_tab.wb.CanGoForward)
            {
                izbran_tab.wb.GoForward();
            }
        }

        private void Go_reload(object sender, RoutedEventArgs e)
        {
            Browsertab izbran_tab = browser_tabs.SelectedItem as Browsertab;
            izbran_tab.wb.Refresh();
        }

        private void open_file_Click(object sender, RoutedEventArgs e)
        {
            

                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "XML Files|*.xml";

                if (openFileDialog.ShowDialog() == true)
                {
                    try
                    {
                        string imeDatoteke = openFileDialog.FileName;
                        Shrani.preberi_vse_inport(imeDatoteke);
                        napolni_zazn_tab();
                        pripravi_okno();
                        tab_bar.Items.Refresh();
                        
                    }
                    catch
                    {
                        MessageBox.Show("Xml je napacne oblike");
                    }
                }
        }
    }
}


//backup -------------------------------------------------------

//int tabs_open = 1;
//string home_page = "http://www.google.com/";

//----------

//Uri url = new Uri(home_page);
//WebBrowser nov_brskalnik = new WebBrowser();
//nov_brskalnik.Source = url;
//TabItem newTabItem = new TabItem
//{
//    Header = home_page,
//    Name = "tab" + tabs_open,
//    Width = 50,


//};

//tabs_open++;
//browser_tabs.Items.Add(newTabItem);


//static----------------------------------------------------------
//TabItem nov_zavihek = new TabItem();
//nov_zavihek.Content = new Browsertab();
//nov_zavihek.Header = "zavihek" + stevilo_zavihkov;
//browser_tabs.Items.Add(nov_zavihek);