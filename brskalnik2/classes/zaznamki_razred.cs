﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace brskalnik2.classes
{


    public class Zazn
    {

        public string Web_name { get; set; }
        public string Web_url { get; set; }
        public string Web_time = DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss");
        public string Picture_path { get; set; }
        public string type { get; set; }

        public static int stevilo_zaznamkov = 0;

    
        public Zazn()
        {
            Web_name = "";
            Web_url = "";
            Web_time = DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss");
            Picture_path = "icons/icons/link.png";
            type = "other";
        }
        public Zazn(string web_name, string web_url)
        {
            Web_name = web_name;
            Web_url = web_url;

            stevilo_zaznamkov++;
        }



        public string Print()
        {
            return "ime strani: " + Web_name + " url strani: " + Web_url;
        }

        public string Print_kat()
        {
            return "kategorija: "+type+" > ime strani: " + Web_name + " url strani: " + Web_url;
        }

        public void Print_to_console()
        {
            Console.WriteLine(stevilo_zaznamkov + " ime strani: " + Web_name);
            Console.WriteLine(" url strani: " + Web_url);
            Console.WriteLine("cas ogleda strani: " + Web_time);
        }





    }

    public class Tip_zaznamka
    {
        public Tip_zaznamka()
        {
            kategorija = "null";
            zaznamki_kategorije = new ObservableCollection<Zazn>();
        }

        public Tip_zaznamka(string kategorija, ObservableCollection<Zazn> zaznamki_kategorije)
        {
            this.kategorija = kategorija;
            this.zaznamki_kategorije = zaznamki_kategorije;
        }

        public string kategorija { get; set; }
        public ObservableCollection<Zazn> zaznamki_kategorije { get; set; }
    }

    //public class Prikaz_zazn
    //{
    //    private static ObservableCollection<string> zaznamki_imen = new ObservableCollection<string>();
    //    private static ObservableCollection<string> zaznamki_linkov = new ObservableCollection<string>();


    //}




}
