﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using brskalnik2.classes;
using System.Collections.ObjectModel;
using System.Windows;


namespace brskalnik2.classes
{

    public class Shrani
    {
        public Shrani()
        {
            kako_zgleda = new Nastavitve();
            list_zaznamkov = new ObservableCollection<Zazn>();
            list_zgodovina = new ObservableCollection<Zgoda>();
            list_zaznamkov_tab = new ObservableCollection<Zazn>();
        }

        public Shrani(Nastavitve kako_zgleda, ObservableCollection<Zgoda> list_zgodovina, ObservableCollection<Zazn> list_zaznamkov, ObservableCollection<Zazn> list_zaznamkov_tab)
        {
            this.kako_zgleda = kako_zgleda;
            this.list_zgodovina = list_zgodovina;
            this.list_zaznamkov = list_zaznamkov;
            this.list_zaznamkov_tab = list_zaznamkov_tab;
        }

        public Shrani(Shrani objekt)
        {
            this.kako_zgleda = objekt.kako_zgleda;
            this.list_zgodovina = objekt.list_zgodovina;
            this.list_zaznamkov = objekt.list_zaznamkov;
            this.list_zaznamkov_tab = objekt.list_zaznamkov_tab;
        }


        public  Nastavitve kako_zgleda { get; set; }
        public  ObservableCollection<Zgoda> list_zgodovina  { get; set; }
        public  ObservableCollection<Zazn> list_zaznamkov  { get; set; }
        public  ObservableCollection<Zazn> list_zaznamkov_tab  { get; set; }



        public static void Add_to_zazn_tab(Zazn zacasen)
        {
            MainWindow.zapisi.list_zaznamkov_tab.Add(zacasen);
            napisi_vse();

        }


        public static void Add_to_zazn(Zazn zacasen)
        {
            MainWindow.zapisi.list_zaznamkov.Add(zacasen);
            napisi_vse();

        }
        public static void Add_to_zgod(Zgoda zacasen)
        {
            MainWindow.zapisi.list_zgodovina.Add(zacasen);
            napisi_vse();

        }



        public static void preberi_vse()
        {
            MainWindow.zapisi=Datoteka_bralec.bralec<Shrani>("nastavitve.xml");

        }

        public static void preberi_vse_inport( string ime)
        {
            MainWindow.zapisi = Datoteka_bralec.bralec<Shrani>(ime);

        }

        public static void napisi_vse()
        {
            Datoteka.Zapisovalec(MainWindow.zapisi, "nastavitve.xml");
        }

    }


    public class Datoteka
    {

        //public static string filename="KKKKKKVVVVVVVVVAAAAAAA.xml";

        public static void Zapisovalec(object obj, string filename)
        {
            XmlSerializer sr = new XmlSerializer(obj.GetType());
            try
            {
                TextWriter writer = new StreamWriter(filename);
                sr.Serialize(writer, obj);
                writer.Close();
            }
            catch (Exception ex)
            {

            }
        }

    }

    public class Datoteka_bralec
    {

        public static T bralec <T> (string filename)where T:new()
        {
            dynamic result = new T();
            XmlSerializer xs = new XmlSerializer(typeof(T));
            
            try
            {
                FileStream reader = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
                result = (T)xs.Deserialize(reader);
                reader.Close();
            }
            catch (Exception ex)
            {

            }
            return (T)result;
        }
    }



}
