﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace brskalnik2.classes
{


    public class Nastavitve
    {
        //private static Zazn homepage = new Zazn("google", Preveri_link.Is_link("").ToString());

        //public static Zazn Homepage { get => homepage; set => homepage = value; }

        public string homepage { get; set; }
        public bool show_bar { get; set; }




        public Nastavitve()
        {
            homepage = "http://www.google.com";
            show_bar = true;
        }

        public Nastavitve(string homepage, bool show_bar)
        {
            this.homepage = homepage;
            this.show_bar = show_bar;
        }
    }



    public class Preveri_link { 
        public static  Uri Is_link(string preveri)
        {
            if (Uri.IsWellFormedUriString(preveri, UriKind.Absolute))
            {
                return new Uri(preveri, UriKind.Absolute);
            }
            return new Uri("https://www.google.com/search?q=" + preveri, UriKind.Absolute);
        }
    }

}
