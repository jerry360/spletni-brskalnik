﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace brskalnik2.classes
{
    public class Zgoda
    {

        public string Web_name { get; set; }
        public string Web_url { get; set; }
        public string Web_time = DateTime.Now.ToString("yyyy.MM.dd hh:mm:ss");

        public static int stevilo_zgodovin = 0;



        public Zgoda(string web_name, string web_url)
        {
            Web_name = web_name;
            Web_url = web_url;

            stevilo_zgodovin++;
        }

        public Zgoda()
        {
            
        }

        public string Print()
        {
            return "ime strani: " + Web_name + " url strani: " + Web_url + " cas ogleda strani: " + Web_time;
        }

        public void Print_to_console()
        {
            Console.WriteLine(stevilo_zgodovin + " ime strani: " + Web_name);
            Console.WriteLine(" url strani: " + Web_url);
            Console.WriteLine("cas ogleda strani: " + Web_time);
        }
    }





}
